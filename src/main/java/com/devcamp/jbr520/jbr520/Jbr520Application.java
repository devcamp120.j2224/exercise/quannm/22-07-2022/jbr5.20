package com.devcamp.jbr520.jbr520;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr520Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr520Application.class, args);
	}

}
