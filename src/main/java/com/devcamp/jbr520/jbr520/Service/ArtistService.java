package com.devcamp.jbr520.jbr520.Service;

import java.util.ArrayList;

import com.devcamp.jbr520.jbr520.model.Artist;

public class ArtistService {
    public static ArrayList<Artist> getListArtist() {
        ArrayList<Artist> listArtist = new ArrayList<>();
        Artist artist1 = new Artist(1, "Marry", AlbumService.getAlbumA());
        Artist artist2 = new Artist(2, "John", AlbumService.getAlbumB());
        Artist artist3 = new Artist(3, "David", AlbumService.getAlbumC());
        listArtist.add(artist1);
        listArtist.add(artist2);
        listArtist.add(artist3);
        return listArtist;
    }
}
