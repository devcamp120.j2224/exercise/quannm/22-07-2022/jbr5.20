package com.devcamp.jbr520.jbr520.Service;

import java.util.ArrayList;

import com.devcamp.jbr520.jbr520.model.Album;

public class AlbumService {
    public static ArrayList<Album> getAlbumA() {
        ArrayList<Album> listAlbumA = new ArrayList<>();
        Album album1 = new Album(1, "AAA", new ArrayList<String>());
        Album album2 = new Album(2, "BBB", new ArrayList<String>());
        Album album3 = new Album(3, "CCC", new ArrayList<String>());
        listAlbumA.add(album1);
        listAlbumA.add(album2);
        listAlbumA.add(album3);
        return listAlbumA;
    }

    public static ArrayList<Album> getAlbumB() {
        ArrayList<Album> listAlbumB = new ArrayList<>();
        Album album4 = new Album(4, "DDD", new ArrayList<String>());
        Album album5 = new Album(5, "EEE", new ArrayList<String>());
        Album album6 = new Album(6, "FFF", new ArrayList<String>());
        listAlbumB.add(album4);
        listAlbumB.add(album5);
        listAlbumB.add(album6);
        return listAlbumB;
    }

    public static ArrayList<Album> getAlbumC() {
        ArrayList<Album> listAlbumC = new ArrayList<>();
        Album album7 = new Album(7, "GGG", new ArrayList<String>());
        Album album8 = new Album(8, "HHH", new ArrayList<String>());
        Album album9 = new Album(9, "III", new ArrayList<String>());
        listAlbumC.add(album7);
        listAlbumC.add(album8);
        listAlbumC.add(album9);
        return listAlbumC;
    }

    public static ArrayList<Album> getAllAlbum() {
        ArrayList<Album> listAllAlbum = new ArrayList<>();
        listAllAlbum.addAll(getAlbumA());
        listAllAlbum.addAll(getAlbumB());
        listAllAlbum.addAll(getAlbumC());
        return listAllAlbum;
    }
}
