package com.devcamp.jbr520.jbr520.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr520.jbr520.Service.AlbumService;
import com.devcamp.jbr520.jbr520.model.Album;

@RestController
public class AlbumController {
    @CrossOrigin
    @GetMapping("/album-info")
    public Album requestAlbum(@RequestParam(required = true, name = "albumId") int id) {
        ArrayList<Album> listAlb = AlbumService.getAllAlbum();
        Album albumFound = new Album();
        for (Album album : listAlb) {
            if (album.getId() == id) {
                albumFound = album;
            }
        }
        return albumFound;
    }
}
