package com.devcamp.jbr520.jbr520.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr520.jbr520.Service.ArtistService;
import com.devcamp.jbr520.jbr520.model.Artist;

@RestController
public class ArtistController {
    @CrossOrigin
    @GetMapping("/artists")
    public ArrayList<Artist> getArtists() {
        ArrayList<Artist> listArt = ArtistService.getListArtist();
        return listArt;
    }

    @CrossOrigin
    @GetMapping("/artist-info")
    public Artist requestArtist(@RequestParam(required = true, name = "artistId") int id) {
        ArrayList<Artist> listArt = ArtistService.getListArtist();
        Artist artistFound = new Artist();
        for (Artist artist : listArt) {
            if (artist.getId() == id) {
                artistFound = artist;
            }
        }
        return artistFound;
    }
}
